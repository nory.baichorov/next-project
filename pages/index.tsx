import axios from 'axios';
import React, { useState } from "react";
import { GetStaticProps } from "next";

import { withLayout } from "../layout/Layout";
import { MenuItem } from "../interfaces/menu.interface";
import { Button, Htag, Ptag, Tag, Rating } from "../components";

function Home({ menu }: HomeProps): JSX.Element {
    const [rating, setRating] = useState<number>(4);

    return (
        <>
            <Htag tag="h1">Заголовок</Htag>
            <Button appearance={"primary"}>button</Button>
            <Button appearance={"ghost"} arrow='right'>button</Button>
            <Ptag>paragraph</Ptag>
            <Tag>S ghost</Tag>
            <Tag size='m'>M ghost</Tag>
            <Rating rating={rating} isEditable setRating={setRating} />
            <ul>
                {menu.map(m => <li key={m._id.secondCategory}>{m._id.secondCategory}</li>)}
            </ul>
        </>
    );
}

// @ts-ignore
export default withLayout(Home);

export const getStaticProps: GetStaticProps<HomeProps> = async () => {
    const firstCategory = 0;
    const { data: menu } = await axios.post<MenuItem[]>(process.env.NEXT_PUBLIC_DOMAIN + '/api/top-page/find', {
        firstCategory
    });

    return {
        props: {
            menu,
            firstCategory,
        }
    }
}

interface HomeProps extends Record<string, unknown> {
    menu: MenuItem[];
    firstCategory: number;
}
