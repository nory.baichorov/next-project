import React from 'react';
import cn from 'classnames';

import styles from './Tag.module.css';
import { TagProps } from "./Tag.props";

export const Tag = ({ href, color = 'ghost', size = 's', children, className, ...props }: TagProps): JSX.Element => {
    return (
        <div
          className={cn(styles.tag, className, {
            [styles.s]: size == 's',
            [styles.m]: size == 'm',
            [styles.ghost]: color == 'ghost',
            [styles.primary]: color == 'primary',
            [styles.green]: color == 'green',
            [styles.grey]: color == 'grey',
            [styles.red]: color == 'red',
          })}
          {...props}
        >
          {
            href
            ? <a href={href}>{children}</a>
            : <>{children}</>
          }
        </div>
    );
};
