import React, { useEffect, useState } from 'react';
import cn from 'classnames';

import styles from './Rating.module.css';
import { RatingProps } from "./Rating.props";
import StarIcon from '../../public/star_logo.svg';

export const Rating = ({ className, isEditable = false, rating, setRating, ...props }: RatingProps): JSX.Element => {
    const [ratingArray, setRatingArray] = useState<JSX.Element[]>(new Array(5).fill(<></>));

    useEffect(() => {
      ratingConstructor(rating);
    }, [rating]);

    const changeDisplay = (i: number) => {
        if (!isEditable) {
            return;
        }
        ratingConstructor(i);
    };

    const changeRating = (i: number) => {
        if (!isEditable || !setRating) {
            return;
        }
        setRating(i);
    }

    const ratingConstructor = (currentRating: number) => {
      const updatedArray = ratingArray.map((icon: JSX.Element, index: number) => {
         return (
            <span
              className={cn(styles.star, className, {
                  [styles.filled]: index < currentRating,
                  [styles.editable]: isEditable
              })}
              onMouseEnter={() => changeDisplay(index + 1)}
              onMouseLeave={() => changeDisplay(rating)}
              onClick={() => changeRating(index + 1)}
            >
              <StarIcon  />
            </span>
         );
      });
      setRatingArray(updatedArray);
    };

    return (
        <div {...props}>
          {ratingArray.map((icon, index) => <span key={index}>{icon}</span>)}
        </div>
    );
};
